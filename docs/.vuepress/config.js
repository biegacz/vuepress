module.exports = {
  title: 'Vuepress',
  description: 'what',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/guide/' },
      { text: 'Config Page', link: '/config' },
      { text: 'Config Test', link: '/test' },
    ]
  }
}
